using System.Linq;
using Castle.Core.Internal;
using KCore.Common.Data;
using Shouldly;
using Xunit;

namespace K.Approval.Api.Test.Common
{
    public class ListParameterTest
    {
        [Fact]
        public void ConvertToArrayShouldBeSuccess()
        {
            var f = new ListParameter
            {
                WhereTerm.Default("value", "column"), WhereTerm.Default("value2", "column2", SqlOperator.NotEqual)
            };

            var r = f.ToArray();
            
            r.Length.ShouldBe(2);
            var c = r.Find(x => x.ColumnName == "column");
            c.ShouldNotBeNull();
            c.ColumnName.ShouldBe("column");
            c.Value.ShouldBe("value");
        }
    }
}