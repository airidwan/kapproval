using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class WorkflowBuilder: BaseFluentBuilder<Workflow>
    {
        public WorkflowBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }
        
        public WorkflowBuilder WithWorkflowCode(string data)
        {
            SetProperty(x => x.WorkflowCode, data);
            return this;
        }
        
        public WorkflowBuilder WithName(string data)
        {
            SetProperty(x => x.WorkflowName, data);
            return this;
        }
    }
}