using System;
using K.Approval.Api.DataContext;
using KCore.Common.Test;

namespace K.Approval.Api.Test.Builders
{
    public class UserProfileBuilder : BaseFluentBuilder<UserProfile>
    {
        public UserProfileBuilder()
        {
            SetProperty(x => x.Id, Guid.NewGuid());
            SetProperty(x => x.RowStatus, RowStatus);
        }
        
        public UserProfileBuilder WithCompany(Guid data)
        {
            SetProperty(x => x.CompanyId, data);
            return this;
        }
        
        public UserProfileBuilder WithEmail(string data)
        {
            SetProperty(x => x.Email, data);
            return this;
        }

        public UserProfileBuilder WithEmployeeCode(string data)
        {
            SetProperty(x => x.EmployeeCode, data);
            return this;
        }

    }
}