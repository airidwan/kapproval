using System.Threading.Tasks;
using K.Approval.Api.DataContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Shouldly;
using Xunit;

namespace K.Approval.Api.Test.Infrastructures
{
    public class DatabaseTest
    {
        private readonly WorkflowDataContext _workflowDataContext;
        public DatabaseTest()
        {
            var contextOptions = new DbContextOptionsBuilder<WorkflowDataContext>()
                .UseInMemoryDatabase("TestPortalDatabase")
                .ConfigureWarnings(x=>x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;
            _workflowDataContext = new WorkflowDataContext(contextOptions);
        }

        [Fact]
        public async Task CanConnectShouldBeTrue()
        {
            var result = await _workflowDataContext.Database.CanConnectAsync();
            result.ShouldBe(true);
        }
        
        [Fact]
        public async Task CanDoTransactionShouldBeTrue()
        {
            var result = await _workflowDataContext.Database.BeginTransactionAsync();
            
            result.Commit();
        }
    }
}