using KCore.Common.Fault;

namespace K.Approval.Api.Common
{
    public static class ProcessError
    {
        public static ErrorDetail InvalidRequestAction => new ErrorDetail{ Description = "Kesalahan, Request Action tidak valid", ErrorCode = 601};
        public static ErrorDetail InvalidModule => new ErrorDetail{ Description = "Kesalahan, ModuleId tidak valid atau module sudah tidak aktif", ErrorCode = 604};
        public static ErrorDetail InvalidConfigurationProcessActivity => new ErrorDetail{ Description = "Tidak ada Konfigurasi Process Activity", ErrorCode = 640};
        public static ErrorDetail InvalidConfigurationProcessActivityIndex => new ErrorDetail{ Description = "Invalid Konfigurasi Activity Index", ErrorCode = 641};
        public static ErrorDetail InvalidApprovalInbox  => new ErrorDetail{ Description = "Kesalahan, Approval tidak ditemukan", ErrorCode = 644};

        public static ErrorDetail InvalidRequestNumber(string requestNumber)
        {
            return new ErrorDetail {Description = "Tidak Ada Process " + requestNumber, ErrorCode = 642};
        }
        
        public static ErrorDetail InvalidProcessAuthorization(string employeeCode)
        {
            return new ErrorDetail {Description = $"User {employeeCode} Tidak Memiliki Hak Untuk Membuat Ticket Approval", ErrorCode = 643};
        }
        public static ErrorDetail InvalidProcessActivity  => new ErrorDetail{ Description = "Kesalahan, Activity Proses tidak ditemmukan", ErrorCode = 645};
        public static ErrorDetail InvalidRequestStatus  => new ErrorDetail{ Description = "Kesalahan, Status Proses tidak ditemmukan", ErrorCode = 646};
        
        public static ErrorDetail DocumentAlreadyProcessedByAnotherUser(string actorName, string documentNumber, string status)
        {
            return new ErrorDetail {Description = $"Request Dokumen No. {documentNumber} dengan status {status} Telah Diproses oleh {actorName}", ErrorCode = 647};
        }
        
        public static ErrorDetail UnauthorizedApproval  => new ErrorDetail{ Description = "Kesalahan, Anda tidak memiliki hak untuk memberikan persetujuan", ErrorCode = 648};
        public static ErrorDetail InvalidFirstIndexProcessActivity  => new ErrorDetail{ Description = "Kesalahan, Activity Proses tidak dapat diulang", ErrorCode = 649};
        public static ErrorDetail InvalidCreatorRequestNotFound  => new ErrorDetail{ Description = "Kesalahan, dokumen tidak dapat direvisi karena data pengajuan tidak ditemukan", ErrorCode = 649};
    }
}