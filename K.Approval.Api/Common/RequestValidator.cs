using System.Threading.Tasks;
using FluentValidation;
using K.Approval.Api.Infrastructures;
using KCore.Common.Fault;

namespace K.Approval.Api.Common
{
    public abstract class RequestValidator<TRequest> : AbstractValidator<TRequest>, IRequestValidator<TRequest>
    {
        public abstract Task<ValidationResult> InternalValidate(TRequest request);

        public virtual int Order => 1;

        private ValidationResult FluentValidate(TRequest request)
        {
            ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;
            ValidatorOptions.DisplayNameResolver = (type, memberInfo, expression) => memberInfo.Name;

            var results = base.Validate(request);
            if (results.IsValid) return null;

            foreach (var failure in results.Errors)
            {
                if (!(failure.CustomState is ValidationResult msg)) continue;

                if (string.IsNullOrWhiteSpace(msg.ErrorDescription)) msg.ErrorDescription = failure.ErrorMessage;
                return msg;
            }
            return null;
        }
    }
}