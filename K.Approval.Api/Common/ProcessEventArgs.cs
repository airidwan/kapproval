using System;

namespace K.Approval.Api.Common
{
    public class ProcessEventArgs : EventArgs
    {
        public ProcessEventArgs()
        {
            NoAction = false;
        }
        public string NewStatus { get; set; }
        public string OldStatus { get; set; }
        public bool NoAction { get; set; }
        public string DisplayStatus { get; set; }
    }
}