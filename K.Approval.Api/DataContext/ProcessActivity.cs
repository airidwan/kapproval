using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class ProcessActivity
    {
        public ProcessActivity()
        {
            ProcessActivityActors = new HashSet<ProcessActivityActor>();
            RequestActivities = new HashSet<RequestActivity>();
            RequestInboxes = new HashSet<RequestInbox>();
            RequestStatuses = new HashSet<RequestStatus>();
        }
    
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid ProcessRequestId { get; set; }
        [Required]
        public int ActivityIndex { get; set; }
        [Required]
        [StringLength(128)]
        public string SubjectName { get; set; }
        [StringLength(128)]
        public string ViewSubject { get; set; }
        [StringLength(128)]
        public string PostSubject { get; set; }
        [StringLength(128)]
        public string RejectMessage { get; set; }
        [StringLength(128)]
        public string ReviseMessage { get; set; }
        [Required]
        [StringLength(64)]
        public string NewStatus { get; set; }
        [Required]
        [StringLength(64)]
        public string DisplayName { get; set; }
        public double StartValue { get; set; }
        public double EndValue { get; set; }
        [StringLength(12)]
        public string SlaType { get; set; }
        public int? SlaTime { get; set; }
        public byte UrlActionType { get; set; }
        [StringLength(128)]
        public string UrlAction { get; set; }
        [StringLength(128)]
        public string ApprovalJavascriptAction { get; set; }
        [StringLength(128)]
        public string ViewJavascriptAction { get; set; }
        [StringLength(128)]
        public string Other01JavascriptAction { get; set; }
        [StringLength(128)]
        public string Other02JavascriptAction { get; set; }
        [StringLength(128)]
        public string Other03JavascriptAction { get; set; }
        [StringLength(128)]
        public string Other04JavascriptAction { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual ProcessRequest ProcessRequest { get; set; }
        public virtual ICollection<ProcessActivityActor> ProcessActivityActors { get; set; }
        public virtual ICollection<RequestActivity> RequestActivities { get; set; }
        public virtual ICollection<RequestInbox> RequestInboxes { get; set; }
        public virtual ICollection<RequestStatus> RequestStatuses { get; set; }
    }
}