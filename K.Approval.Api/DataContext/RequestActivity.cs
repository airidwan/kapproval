using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class RequestActivity
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid ProcessActivityId { get; set; }
        [StringLength(64)]
        public string ObjectKey { get; set; }
        [StringLength(32)]
        public string RequestNumber { get; set; }
        [StringLength(64)]
        public string DocumentName { get; set; }
        [StringLength(32)]
        public string DocumentNumber { get; set; }
        public int ActivityIndex { get; set; }
        [StringLength(24)]
        public string ActorCode { get; set; }
        [StringLength(64)]
        public string ActorName { get; set; }
        [StringLength(64)]
        public string RequestStatus { get; set; }
        [StringLength(64)]
        public string DisplayStatus { get; set; }
        [StringLength(128)]
        public string SubjectName { get; set; }
        public byte IsComplete { get; set; }
        [StringLength(64)]
        public string ActionName { get; set; }
        public DateTime? ActionDate { get; set; }
        [StringLength(12)]
        public string SlaType { get; set; }
        public int? SlaTime { get; set; }
        public string Description { get; set; }
        [StringLength(24)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual ProcessActivity ProcessActivity { get; set; }
    }
}