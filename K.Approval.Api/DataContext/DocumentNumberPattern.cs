using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class DocumentNumberPattern
    {
        public DocumentNumberPattern()
        {
            DocumentNumberLogs = new HashSet<DocumentNumberLog>();
        }

        public ICollection<DocumentNumberLog> DocumentNumberLogs { get; set; }
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid DocumentNumberId { get; set; }
        [Required]
        public int IndexPattern { get; set; }
        [Required]
        public DocumentPatternType PatterType { get; set; }
        [Required]
        [StringLength(12)]
        public string FixedValue { get; set; }
        [Required]
        public int LastIndex { get; set; }
        [Required]
        public int LengthNumber { get; set; }
        [Required]
        public bool ResetMonthly { get; set; }
        [Required]
        public bool ResetAnnually { get; set; }
        [Required]
        [StringLength(1)]
        public string Separator { get; set; }
        [Required]
        [StringLength(24)]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [StringLength(24)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual DocumentNumber DocumentNumber { get; set; }
    }
}