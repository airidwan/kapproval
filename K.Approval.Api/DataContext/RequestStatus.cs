using System;
using System.ComponentModel.DataAnnotations;

namespace K.Approval.Api.DataContext
{
    public class RequestStatus
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public byte RowStatus { get; set; }
        [Required]
        public byte[] RowVersion { get; set; }
        [Required]
        public Guid ProcessActivityId { get; set; }
        [Required]
        [StringLength(64)]
        public string ObjectKey { get; set; }
        [Required]
        [StringLength(24)]
        public string RequestNumber { get; set; }
        [StringLength(64)]
        [Required]
        public string DocumentName { get; set; }
        [StringLength(32)]
        [Required]
        public string DocumentNumber { get; set; }
        [StringLength(64)]
        public string NewRequestStatus { get; set; }
        [StringLength(64)]
        [Required]
        public string DisplayStatus { get; set; }
        [StringLength(24)]
        [Required]
        public string ActorCode { get; set; }
        [StringLength(64)]
        [Required]
        public string ActorName { get; set; }
        [Required]
        public byte IsComplete { get; set; }
        public DateTime? CompleteDate { get; set; }
        [StringLength(128)]
        public string Subject { get; set; }
        [Required]
        public DateTime RequestDate { get; set; }
        [StringLength(24)]
        [Required]
        public string LastAssignTo { get; set; }
        [Required]
        public DateTime LastAssignDate { get; set; }
        public DateTime? CommitmentDate { get; set; }
        [StringLength(12)]
        public string SlaType { get; set; }
        public int? SlaTime { get; set; }
        public string Notes { get; set; }
        [StringLength(24)]
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    
        public virtual ProcessActivity ProcessActivity { get; set; }
    }
}