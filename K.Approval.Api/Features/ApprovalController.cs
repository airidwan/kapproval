using System.Threading.Tasks;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace K.Approval.Api.Features
{
    public class ApprovalController : Controller
    {
        private readonly IMediator _mediator;
        public ApprovalController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [HttpPut]
        [Route("api/approval/baru")]
        public async Task<IActionResult> Post([FromBody]SubmitProcess.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
        
        [HttpPost]
        [Route("api/approval/inbox")]
        public async Task<IActionResult> GetInbox([FromBody]GetInbox.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
        
        [HttpPut]
        [Route("api/approval/setujui")]
        public async Task<IActionResult> Approval([FromBody]Approval.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
        
        [HttpPut]
        [Route("api/approval/tolak")]
        public async Task<IActionResult> Rejected([FromBody]Rejected.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
        
        [HttpPut]
        [Route("api/approval/revisi")]
        public async Task<IActionResult> Revised([FromBody]Revise.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
    }
}