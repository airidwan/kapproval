using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using K.Approval.Api.Common;
using K.Approval.Api.DataContext;
using KCore.Common.Extensions;
using KCore.Common.Fault;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.Features.Rejected
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly WorkflowDataContext _workflowDataContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public Handler(IHttpContextAccessor httpContextAccessor, WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
            _httpContextAccessor = httpContextAccessor;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var inbox = await
                _workflowDataContext.RequestInboxes.FirstOrDefaultAsync(
                    c => c.RowStatus == 0 && c.Id == request.InboxId, cancellationToken);
            if (inbox == null) throw new ApiException(DefaultError.DataNotFound);
            inbox.IsComplete = true;
            inbox.CompleteDate = DateTime.Now;
            inbox.ModifiedBy = request.GetAccessor();
            inbox.ModifiedDate = DateTime.Now;
            inbox.HasView = true;
            inbox.ViewDate = DateTime.Now;
            inbox.ViewNetworkInfo = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            
            
            request.ObjectId = inbox.ObjectId;
            request.RequestNumber = inbox.RequestNumber;


            var processActivity =
                await _workflowDataContext.ProcessActivities.FirstOrDefaultAsync(x =>
                    x.RowStatus == 0 && x.Id == inbox.ProcessActivityId, cancellationToken);

                if (processActivity == null) throw new ApiException(ProcessError.InvalidProcessActivity);

            var requestActivities = await _workflowDataContext.RequestActivities.Where(x =>
                x.ProcessActivityId == inbox.ProcessActivityId && x.RequestNumber
                == inbox.RequestNumber && x.RowStatus == 0).ToListAsync(cancellationToken);

            foreach (var activity in requestActivities)
            {
                activity.IsComplete = 1;
                activity.ModifiedBy = request.GetAccessor();
                activity.ModifiedDate = DateTime.Now;
            }

            var requestActivity = requestActivities.FirstOrDefault(c => c.ActorCode == request.UserProfile.EmployeeCode);
            if (requestActivity == null) throw new ApiException(ProcessError.UnauthorizedApproval);

            requestActivity.IsComplete = 1;
            requestActivity.ActionDate = inbox.CompleteDate;
            requestActivity.ActionName = request.ActionName;
            requestActivity.ActorCode = request.UserProfile.EmployeeCode;
            requestActivity.ActorName = request.UserProfile.FullName;
            requestActivity.ModifiedBy = request.GetAccessor();
            requestActivity.Description = request.Description;
            requestActivity.ModifiedDate = DateTime.Now;
            requestActivity.SubjectName = ApprovalHelper.CreateRejectMessage(processActivity, requestActivity, request.Action, request.UserProfile.FullName); 

            var requestStatus = await _workflowDataContext.RequestStatuses.FirstOrDefaultAsync(
                    c => c.ProcessActivityId == processActivity.Id && c.RequestNumber == inbox.RequestNumber && c.RowStatus == 0, cancellationToken);
            if (requestStatus == null) throw new ApiException(ProcessError.InvalidRequestStatus);
            if (requestStatus.IsComplete == 1)
                throw new ApiException(ProcessError.DocumentAlreadyProcessedByAnotherUser(requestStatus.ActorName, requestStatus.DocumentNumber, requestStatus.DisplayStatus));

            requestStatus.ProcessActivityId = processActivity.Id;
            requestStatus.LastAssignDate = DateTime.Now;
            requestStatus.LastAssignTo = request.UserProfile.FullName;
            requestStatus.DisplayStatus = ApprovalHelper.SetDisplayName(processActivity, request.Action.ToRequestAction());
            requestStatus.NewRequestStatus = ApprovalHelper.SetRequestStatus(processActivity, request.Action.ToRequestAction());
            requestStatus.ModifiedBy = request.GetAccessor();
            requestStatus.ModifiedDate = DateTime.Now;
            requestStatus.Notes = request.Description;

            requestStatus.Subject = ApprovalHelper.CreateRejectMessage(processActivity, requestActivity, request.Action, request.UserProfile.FullName); 
            requestStatus.SlaTime = processActivity.SlaTime;
            requestStatus.SlaType = processActivity.SlaType;

            requestStatus.IsComplete = 1;
            requestStatus.CompleteDate = DateTime.Now;



            var activityActors = await _workflowDataContext.ProcessActivityActors
                .Where(c => c.ProcessActivityId == processActivity.Id).ToListAsync(cancellationToken);
            var emailTo = "";
            foreach (var activityActor in activityActors)
            {
                emailTo += activityActor.ActorEmail + ";";
                var requestInbox = new RequestInbox
                {
                    Id = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    CreatedBy = request.GetAccessor(),
                    RowStatus = 0,
                    ModifiedBy = string.Empty,
                    ModifiedDate = DateTime.Now,
                    CommitmentDate = null,
                    RequestDate = requestStatus.RequestDate,
                    RequestNumber = request.RequestNumber,
                    DisplayStatus = ApprovalHelper.SetDisplayName(processActivity, request.Action.ToRequestAction()),
                    RequestStatus = ApprovalHelper.SetRequestStatus(processActivity, request.Action.ToRequestAction()),
                    ActorNameRequester = requestStatus.ActorName,
                    Subject = ApprovalHelper.CreateRejectMessage(processActivity, requestActivity, request.Action, request.UserProfile.FullName),
                    ActivityUrl = "",
                    AssignDate = DateTime.Now,
                    CompleteDate = DateTime.Now,
                    HasView = false,
                    IsComplete = true,
                    IsDelegatee = false,
                    RequestDelegateFromId = string.Empty,
                    RequestDelegateFromName = string.Empty,
                    ActorCodeAssignee = activityActor.ActorCode,
                    ActorNameAssignee = activityActor.ActorName,
                    ActorCodeRequester = requestStatus.ActorCode,
                    JavascriptAction = processActivity.ViewJavascriptAction,
                    UrlAction = processActivity.UrlAction,
                    UrlActionType = processActivity.UrlActionType,
                    ProcessActivityId = activityActor.ProcessActivityId,
                    Description = request.Description,
                    ObjectId = request.ObjectId,
                    ActionType = InboxActionType.View.ToString(),
                    DocumentName = requestStatus.DocumentName,
                    DocumentNumber = requestStatus.DocumentNumber
                };
                await _workflowDataContext.RequestInboxes.AddAsync(requestInbox, cancellationToken);
            }

            var emailTask = new EmailTask
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                CreatedBy = request.GetAccessor(),
                RowStatus = 0,
                ModifiedBy = "",
                ModifiedDate = DateTime.Now,
                EmailBody = "",
                EmailCc = "",
                EmailFrom = "",
                EmailSubject = ApprovalHelper.CreateRejectMessage(processActivity, requestActivity, request.Action, request.UserProfile.FullName),
                EmailTo = emailTo,
                SourceId = processActivity.Id,
                TaskFrom = processActivity.SubjectName,
            };

            await _workflowDataContext.EmailTasks.AddAsync(emailTask, cancellationToken);

            await _workflowDataContext.SaveChangesAsync(cancellationToken);
            /*OnStatusChanged(new ProcessEventArgs
            {
                NewStatus = requestStatus.NewRequestStatus,
                OldStatus = requestParameter.RequestStatus
            });*/
            return ApiResult<Response>.Ok(new Response
            {
                Url = _httpContextAccessor.HttpContext?.Request?.GetDisplayUrl()
            });
        }
    }
}