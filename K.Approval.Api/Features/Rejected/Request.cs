using System;
using K.Approval.Api.Common;
using KCore.Common.Response;
using MediatR;

namespace K.Approval.Api.Features.Rejected
{
    public class Request : BaseRequest, IRequest<ApiResult<Response>>
    {
        internal static RequestParameter ToRequestParameter(Request request)
        {
            return new RequestParameter
            {
                FullName = request.UserProfile.FullName,
                ObjectKey = request.ObjectId,
                DocumentName = "",
                RequestNumber = request.RequestNumber,
                ActionName = request.Action.ToRequestAction(),
                Description = request.Description,
                UserName = request.UserProfile.EmployeeCode,
                
            };
        }

        public string Action { get; set; }

        public string Description { get; set; }

        public string RequestNumber { get; set; }

        public string ObjectId { get; set; }
        public Guid InboxId { get; set; }
        public string ActionName { get; set; }
        public string DocumentNumber { get; set; }
        public string Email { get; set; }
    }
}