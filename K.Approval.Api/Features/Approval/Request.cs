using System;
using System.Runtime.InteropServices.WindowsRuntime;
using K.Approval.Api.Common;
using KCore.Common.Response;
using MediatR;

namespace K.Approval.Api.Features.Approval
{
    public class Request : BaseRequest, IRequest<ApiResult<Response>>
    {
        public string Action { get; set; }
        public Guid Id { get; set; }
        public string RequestNumber { get; set; }
        public string ObjectId { get; set; }
        public string Description { get; set; }
        
        public string Email { get; set; }

        internal static RequestParameter ToRequestParameter(Request request)
        {
            return new RequestParameter
            {
                FullName = request.UserProfile.FullName,
                ObjectKey = request.ObjectId,
                DocumentName = "",
                RequestNumber = request.RequestNumber,
                ActionName = request.Action.ToRequestAction(),
                Description = request.Description,
                UserName = request.UserProfile.EmployeeCode,
                
            };
        }
    }
}