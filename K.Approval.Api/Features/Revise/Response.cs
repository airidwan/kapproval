using K.Approval.Api.Common;

namespace K.Approval.Api.Features.Revise
{
    public class Response : BaseResponse
    {
        public string NewStatus { get; set; }
        public string OldStatus { get; set; }
    }
}