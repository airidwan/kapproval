using K.Approval.Api.Common;

namespace K.Approval.Api.Features.SubmitProcess
{
    public class Response :BaseResponse
    {
        
        public string RequestNumber { get; set; }
        public string NewStatus { get; set; }
        public string DisplayStatus { get; set; }
    }
}