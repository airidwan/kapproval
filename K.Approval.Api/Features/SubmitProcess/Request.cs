using System;
using K.Approval.Api.Common;
using K.Approval.Api.Infrastructures;
using KCore.Common.Response;
using MediatR;

namespace K.Approval.Api.Features.SubmitProcess
{
    public class Request : BaseRequest, IRequest<ApiResult<Response>>
    {
        public string ModuleId { get; set; }
        public string Email { get; set; }
        public string Action { get; set; }
        public DateTime RequestDate { get; set; }
        public string Description { get; set; }
        public string RequestNumber { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentName { get; set; }
        public string ObjectKey { get; set; }
        
        internal static RequestParameter ToRequestParameter(Request request)
        {
            return new RequestParameter
            {
                FullName = request.UserProfile.FullName,
                ObjectKey = request.ObjectKey,
                DocumentName = request.DocumentName,
                DocumentNumber = request.DocumentNumber,
                RequestNumber = request.RequestNumber,
                ActionName = request.Action.ToRequestAction(),
                Description = request.Description,
                UserName = request.UserProfile.EmployeeCode,
                RequestDate = request.RequestDate,
                ModuleId = request.ModuleId,
                CompanyId = request.Company?.CompanyCode
            };
        }
    }
}