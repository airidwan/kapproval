using System;
using System.Globalization;
using System.Threading.Tasks;
using K.Approval.Api.DataContext;
using Microsoft.EntityFrameworkCore;

namespace K.Approval.Api.Services
{
    public class DocumentNumberService
    {
        private readonly WorkflowDataContext _workflowDataContext;
        public DocumentNumberService(WorkflowDataContext workflowDataContext)
        {
            _workflowDataContext = workflowDataContext;
        }
        
        public async Task<string> GetNewNumber(string documentCode)
        {
            var documentNumber = await _workflowDataContext.DocumentNumbers
                .Include(x=>x.DocumentNumberPatterns)
                .FirstOrDefaultAsync(c => c.DocumentCode.Equals(documentCode));
            if (documentNumber == null)
                throw new Exception("Tidak ada konfigurasi kodifikasi dokumen " + documentCode);

            if (documentNumber.DocumentNumberPatterns.Count == 0)
            {
                throw new Exception("Tidak ada konfigurasi kodifikasi dokumen " + documentCode);
            }
            
            var result = "";
			foreach (var model in documentNumber.DocumentNumberPatterns)
			{
				switch (model.PatterType)
				{
					case DocumentPatternType.Year:
						var year = DateTime.Now.Year.ToString(CultureInfo.InvariantCulture);
						if (model.LengthNumber < year.Length)
							result += year.Substring(model.LengthNumber) + model.Separator.Trim();
						else
							result += year + model.Separator.Trim();
						break;
					case DocumentPatternType.Month:
						var month = DateTime.Now.Month.ToString(CultureInfo.InvariantCulture);
						if (model.LengthNumber == 1)
							result += month + model.Separator.Trim();
						else
							result += DateTime.Now.Month.ToString("D" + 2) + model.Separator.Trim();
						break;
					case DocumentPatternType.FixedValue:
						result += model.FixedValue.Trim() + model.Separator.Trim();
						break;
					default:
						if (model.ResetAnnually || model.ResetMonthly)
						{
							var m = DateTime.Now.Month;
							var y = DateTime.Now.Year;
							DocumentNumberLog documentNumberLog;
							if (model.ResetMonthly)
							{
								documentNumberLog = await 
									_workflowDataContext.DocumentNumberLogs.FirstOrDefaultAsync(
										c =>
											c.MonthNumber == m && c.YearNumber == y &&
											c.DocumentNumberPatternId == model.Id);
							}
							else
							{
								documentNumberLog = await 
									_workflowDataContext.DocumentNumberLogs.FirstOrDefaultAsync(c => c.YearNumber == y && 
									                                                                 c.DocumentNumberPatternId == model.Id);
							}

							if (documentNumberLog == null)
							{
								documentNumberLog = new DocumentNumberLog
								{
									ModifiedDate = DateTime.UtcNow,
									CreatedDate = DateTime.UtcNow,
									Id = Guid.NewGuid(),
									ModifiedBy = "DocNumber",
									RowStatus = 0,
									CreatedBy = "DocNumber",
									DocumentNumberPatternId = model.Id,
									LastIndex = model.LastIndex,
									MonthNumber = m,
									YearNumber = y
								};
								_workflowDataContext.DocumentNumberLogs.Add(documentNumberLog);
								model.LastIndex = 0;
							}
						}

						var lastIndex = model.LastIndex + 1;
						result += lastIndex.ToString("D" + model.LengthNumber) + model.Separator.Trim();
						model.LastIndex = lastIndex;
						model.ModifiedDate = DateTime.UtcNow;
						_workflowDataContext.SaveChanges();
						break;
				}
			}
			return result;
        }
    }
}