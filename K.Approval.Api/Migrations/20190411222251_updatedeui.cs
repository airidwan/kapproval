﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class updatedeui : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TProcessActivityActors");

            migrationBuilder.CreateTable(
                name: "TProcessActivityActor",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ProcessActivityId = table.Column<Guid>(nullable: false),
                    ActorCode = table.Column<string>(nullable: false),
                    ActorName = table.Column<string>(nullable: false),
                    ActorPosition = table.Column<string>(nullable: true),
                    ActionType = table.Column<byte>(nullable: false),
                    ActorEmail = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    TProcessActivityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProcessActivityActor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TProcessActivityActor_TProcessActivity_TProcessActivityId",
                        column: x => x.TProcessActivityId,
                        principalTable: "TProcessActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TProcessActivityActor_TProcessActivityId",
                table: "TProcessActivityActor",
                column: "TProcessActivityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TProcessActivityActor");

            migrationBuilder.CreateTable(
                name: "TProcessActivityActors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessRequestId = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    TProcessActivityId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProcessActivityActors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TProcessActivityActors_TProcessActivity_TProcessActivityId",
                        column: x => x.TProcessActivityId,
                        principalTable: "TProcessActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TProcessActivityActors_TProcessActivityId",
                table: "TProcessActivityActors",
                column: "TProcessActivityId");
        }
    }
}
