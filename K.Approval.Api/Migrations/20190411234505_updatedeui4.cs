﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class updatedeui4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivityActors_TProcessActivitys_TProcessActivityId",
                table: "TProcessActivityActors");

            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivitys_TProcessRequests_TProcessRequestId",
                table: "TProcessActivitys");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessActivitys",
                table: "TProcessActivitys");

            migrationBuilder.RenameTable(
                name: "TProcessActivitys",
                newName: "TProcessActivities");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessActivitys_TProcessRequestId",
                table: "TProcessActivities",
                newName: "IX_TProcessActivities_TProcessRequestId");

            migrationBuilder.AlterColumn<string>(
                name: "SlaType",
                table: "TProcessActivities",
                maxLength: 12,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 24,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessActivities",
                table: "TProcessActivities",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ProcessRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CompanyId = table.Column<string>(maxLength: 24, nullable: false),
                    ModuleId = table.Column<string>(maxLength: 24, nullable: false),
                    ModuleName = table.Column<string>(maxLength: 128, nullable: false),
                    DisplayProcessName = table.Column<string>(maxLength: 128, nullable: false),
                    StartActive = table.Column<DateTime>(nullable: false),
                    EndActive = table.Column<DateTime>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    Notes = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcessRequests_Workflows_WorkflowId",
                        column: x => x.WorkflowId,
                        principalTable: "Workflows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProcessActivities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ProcessRequestId = table.Column<Guid>(nullable: false),
                    ActivityIndex = table.Column<int>(nullable: false),
                    SubjectName = table.Column<string>(maxLength: 128, nullable: false),
                    ViewSubject = table.Column<string>(maxLength: 128, nullable: true),
                    PostSubject = table.Column<string>(maxLength: 128, nullable: true),
                    NewStatus = table.Column<string>(maxLength: 64, nullable: false),
                    DisplayName = table.Column<string>(maxLength: 64, nullable: false),
                    StartValue = table.Column<double>(nullable: false),
                    EndValue = table.Column<double>(nullable: false),
                    SlaType = table.Column<string>(maxLength: 12, nullable: true),
                    SlaTime = table.Column<int>(nullable: true),
                    UrlActionType = table.Column<byte>(nullable: false),
                    UrlAction = table.Column<string>(maxLength: 128, nullable: true),
                    ApprovalJavascriptAction = table.Column<string>(maxLength: 128, nullable: true),
                    ViewJavascriptAction = table.Column<string>(maxLength: 128, nullable: true),
                    Other01JavascriptAction = table.Column<string>(maxLength: 128, nullable: true),
                    Other02JavascriptAction = table.Column<string>(maxLength: 128, nullable: true),
                    Other03JavascriptAction = table.Column<string>(maxLength: 128, nullable: true),
                    Other04JavascriptAction = table.Column<string>(maxLength: 128, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessActivities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcessActivities_ProcessRequests_ProcessRequestId",
                        column: x => x.ProcessRequestId,
                        principalTable: "ProcessRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProcessActivityActors",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ProcessActivityId = table.Column<Guid>(nullable: false),
                    ActorCode = table.Column<string>(maxLength: 24, nullable: false),
                    ActorName = table.Column<string>(maxLength: 64, nullable: false),
                    ActorPosition = table.Column<string>(maxLength: 64, nullable: true),
                    ActionType = table.Column<byte>(nullable: false),
                    ActorEmail = table.Column<string>(maxLength: 256, nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessActivityActors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcessActivityActors_ProcessActivities_ProcessActivityId",
                        column: x => x.ProcessActivityId,
                        principalTable: "ProcessActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RequestActivity",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ProcessActivityId = table.Column<Guid>(nullable: false),
                    ObjectKey = table.Column<string>(maxLength: 64, nullable: true),
                    RequestNumber = table.Column<string>(maxLength: 32, nullable: true),
                    DocumentName = table.Column<string>(maxLength: 64, nullable: true),
                    DocumentNumber = table.Column<string>(maxLength: 32, nullable: true),
                    ActivityIndex = table.Column<int>(nullable: false),
                    ActorCode = table.Column<string>(maxLength: 24, nullable: true),
                    ActorName = table.Column<string>(maxLength: 64, nullable: true),
                    RequestStatus = table.Column<string>(maxLength: 64, nullable: true),
                    DisplayStatus = table.Column<string>(maxLength: 64, nullable: true),
                    SubjectName = table.Column<string>(maxLength: 128, nullable: true),
                    IsComplete = table.Column<byte>(nullable: false),
                    ActionName = table.Column<string>(maxLength: 64, nullable: true),
                    ActionDate = table.Column<DateTime>(nullable: true),
                    SlaType = table.Column<string>(maxLength: 12, nullable: true),
                    SlaTime = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestActivity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestActivity_ProcessActivities_ProcessActivityId",
                        column: x => x.ProcessActivityId,
                        principalTable: "ProcessActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RequestInboxes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ProcessActivityId = table.Column<Guid>(nullable: false),
                    ObjectId = table.Column<string>(maxLength: 64, nullable: false),
                    RequestNumber = table.Column<string>(maxLength: 32, nullable: false),
                    DocumentName = table.Column<string>(maxLength: 64, nullable: false),
                    DocumentNumber = table.Column<string>(maxLength: 32, nullable: false),
                    ActorCodeRequester = table.Column<string>(maxLength: 24, nullable: false),
                    ActorNameRequester = table.Column<string>(maxLength: 64, nullable: false),
                    AssignDate = table.Column<DateTime>(nullable: false),
                    RequestDate = table.Column<DateTime>(nullable: false),
                    Subject = table.Column<string>(maxLength: 128, nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: true),
                    CommitmentDate = table.Column<DateTime>(nullable: true),
                    ActorCodeAssignee = table.Column<string>(maxLength: 24, nullable: true),
                    ActorNameAssignee = table.Column<string>(maxLength: 64, nullable: true),
                    IsComplete = table.Column<bool>(nullable: false),
                    ActivityUrl = table.Column<string>(maxLength: 1024, nullable: false),
                    RequestStatus = table.Column<string>(maxLength: 64, nullable: false),
                    DisplayStatus = table.Column<string>(maxLength: 64, nullable: false),
                    CompleteDate = table.Column<DateTime>(nullable: true),
                    IsDelegatee = table.Column<bool>(nullable: false),
                    RequestDelegateFromId = table.Column<string>(maxLength: 24, nullable: true),
                    RequestDelegateFromName = table.Column<string>(maxLength: 64, nullable: true),
                    HasView = table.Column<bool>(nullable: false),
                    ViewDate = table.Column<DateTime>(nullable: true),
                    ViewNetworkInfo = table.Column<string>(maxLength: 512, nullable: true),
                    UrlActionType = table.Column<byte>(nullable: false),
                    UrlAction = table.Column<string>(maxLength: 512, nullable: false),
                    JavascriptAction = table.Column<string>(maxLength: 256, nullable: false),
                    ActionType = table.Column<string>(maxLength: 32, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestInboxes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestInboxes_ProcessActivities_ProcessActivityId",
                        column: x => x.ProcessActivityId,
                        principalTable: "ProcessActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RequestStatuses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    ProcessActivityId = table.Column<Guid>(nullable: false),
                    ObjectKey = table.Column<string>(maxLength: 64, nullable: false),
                    RequestNumber = table.Column<string>(nullable: false),
                    DocumentName = table.Column<string>(maxLength: 64, nullable: false),
                    DocumentNumber = table.Column<string>(maxLength: 32, nullable: false),
                    NewRequestStatus = table.Column<string>(maxLength: 64, nullable: true),
                    DisplayStatus = table.Column<string>(maxLength: 64, nullable: false),
                    ActorCode = table.Column<string>(maxLength: 24, nullable: false),
                    ActorName = table.Column<string>(maxLength: 64, nullable: false),
                    IsComplete = table.Column<byte>(nullable: false),
                    CompleteDate = table.Column<DateTime>(nullable: true),
                    Subject = table.Column<string>(maxLength: 128, nullable: true),
                    RequestDate = table.Column<DateTime>(nullable: false),
                    LastAssignTo = table.Column<string>(maxLength: 24, nullable: false),
                    LastAssignDate = table.Column<DateTime>(nullable: false),
                    CommitmentDate = table.Column<DateTime>(nullable: true),
                    SlaType = table.Column<string>(maxLength: 12, nullable: true),
                    SlaTime = table.Column<int>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestStatuses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestStatuses_ProcessActivities_ProcessActivityId",
                        column: x => x.ProcessActivityId,
                        principalTable: "ProcessActivities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProcessActivities_ProcessRequestId",
                table: "ProcessActivities",
                column: "ProcessRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcessActivityActors_ProcessActivityId",
                table: "ProcessActivityActors",
                column: "ProcessActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcessRequests_WorkflowId",
                table: "ProcessRequests",
                column: "WorkflowId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestActivity_ProcessActivityId",
                table: "RequestActivity",
                column: "ProcessActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestInboxes_ProcessActivityId",
                table: "RequestInboxes",
                column: "ProcessActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_RequestStatuses_ProcessActivityId",
                table: "RequestStatuses",
                column: "ProcessActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivities_TProcessRequests_TProcessRequestId",
                table: "TProcessActivities",
                column: "TProcessRequestId",
                principalTable: "TProcessRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivityActors_TProcessActivities_TProcessActivityId",
                table: "TProcessActivityActors",
                column: "TProcessActivityId",
                principalTable: "TProcessActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivities_TProcessRequests_TProcessRequestId",
                table: "TProcessActivities");

            migrationBuilder.DropForeignKey(
                name: "FK_TProcessActivityActors_TProcessActivities_TProcessActivityId",
                table: "TProcessActivityActors");

            migrationBuilder.DropTable(
                name: "ProcessActivityActors");

            migrationBuilder.DropTable(
                name: "RequestActivity");

            migrationBuilder.DropTable(
                name: "RequestInboxes");

            migrationBuilder.DropTable(
                name: "RequestStatuses");

            migrationBuilder.DropTable(
                name: "ProcessActivities");

            migrationBuilder.DropTable(
                name: "ProcessRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TProcessActivities",
                table: "TProcessActivities");

            migrationBuilder.RenameTable(
                name: "TProcessActivities",
                newName: "TProcessActivitys");

            migrationBuilder.RenameIndex(
                name: "IX_TProcessActivities_TProcessRequestId",
                table: "TProcessActivitys",
                newName: "IX_TProcessActivitys_TProcessRequestId");

            migrationBuilder.AlterColumn<string>(
                name: "SlaType",
                table: "TProcessActivitys",
                maxLength: 24,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 12,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_TProcessActivitys",
                table: "TProcessActivitys",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivityActors_TProcessActivitys_TProcessActivityId",
                table: "TProcessActivityActors",
                column: "TProcessActivityId",
                principalTable: "TProcessActivitys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TProcessActivitys_TProcessRequests_TProcessRequestId",
                table: "TProcessActivitys",
                column: "TProcessRequestId",
                principalTable: "TProcessRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
