﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class AddDocumentNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DocumentNumbers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    DocumentCode = table.Column<string>(maxLength: 12, nullable: false),
                    DocumentName = table.Column<string>(maxLength: 64, nullable: false),
                    DocumentPattern = table.Column<int>(nullable: false),
                    PrefixChar = table.Column<string>(maxLength: 12, nullable: false),
                    SuffixChar = table.Column<string>(maxLength: 12, nullable: false),
                    LastIndex = table.Column<int>(nullable: false),
                    LengthNumber = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentNumbers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DocumentNumberPatterns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    DocumentNumberId = table.Column<Guid>(nullable: false),
                    IndexPattern = table.Column<int>(nullable: false),
                    PatterType = table.Column<int>(nullable: false),
                    FixedValue = table.Column<string>(maxLength: 12, nullable: false),
                    LastIndex = table.Column<int>(nullable: false),
                    LengthNumber = table.Column<int>(nullable: false),
                    ResetMonthly = table.Column<bool>(nullable: false),
                    ResetAnnually = table.Column<bool>(nullable: false),
                    Separator = table.Column<string>(maxLength: 1, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentNumberPatterns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentNumberPatterns_DocumentNumbers_DocumentNumberId",
                        column: x => x.DocumentNumberId,
                        principalTable: "DocumentNumbers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentNumberLogs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    DocumentNumberPatternId = table.Column<Guid>(nullable: false),
                    YearNumber = table.Column<short>(nullable: false),
                    MonthNumber = table.Column<byte>(nullable: false),
                    LastIndex = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentNumberLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DocumentNumberLogs_DocumentNumberPatterns_DocumentNumberPat~",
                        column: x => x.DocumentNumberPatternId,
                        principalTable: "DocumentNumberPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentNumberLogs_DocumentNumberPatternId",
                table: "DocumentNumberLogs",
                column: "DocumentNumberPatternId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentNumberPatterns_DocumentNumberId",
                table: "DocumentNumberPatterns",
                column: "DocumentNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentNumbers_DocumentCode",
                table: "DocumentNumbers",
                column: "DocumentCode",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentNumberLogs");

            migrationBuilder.DropTable(
                name: "DocumentNumberPatterns");

            migrationBuilder.DropTable(
                name: "DocumentNumbers");
        }
    }
}
