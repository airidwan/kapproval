﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class ProcessRequestTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TProcessRequest",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    CompanyId = table.Column<string>(maxLength: 24, nullable: false),
                    ModuleId = table.Column<string>(maxLength: 24, nullable: false),
                    ModuleName = table.Column<string>(maxLength: 128, nullable: false),
                    DisplayProcessName = table.Column<string>(maxLength: 128, nullable: false),
                    StartActive = table.Column<DateTime>(nullable: false),
                    EndActive = table.Column<DateTime>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    Notes = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatedBy = table.Column<string>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProcessRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TProcessRequest_Workflows_WorkflowId",
                        column: x => x.WorkflowId,
                        principalTable: "Workflows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TProcessActivity",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    TProcessRequestId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TProcessActivity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TProcessActivity_TProcessRequest_TProcessRequestId",
                        column: x => x.TProcessRequestId,
                        principalTable: "TProcessRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TProcessActivity_TProcessRequestId",
                table: "TProcessActivity",
                column: "TProcessRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_TProcessRequest_WorkflowId",
                table: "TProcessRequest",
                column: "WorkflowId");

            migrationBuilder.CreateIndex(
                name: "IX_TProcessRequest_CompanyId_ModuleId",
                table: "TProcessRequest",
                columns: new[] { "CompanyId", "ModuleId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TProcessActivity");

            migrationBuilder.DropTable(
                name: "TProcessRequest");
        }
    }
}
