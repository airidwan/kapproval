﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace K.Approval.Api.Migrations
{
    public partial class updatedeui5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestActivity_ProcessActivities_ProcessActivityId",
                table: "RequestActivity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RequestActivity",
                table: "RequestActivity");

            migrationBuilder.RenameTable(
                name: "RequestActivity",
                newName: "RequestActivities");

            migrationBuilder.RenameIndex(
                name: "IX_RequestActivity_ProcessActivityId",
                table: "RequestActivities",
                newName: "IX_RequestActivities_ProcessActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RequestActivities",
                table: "RequestActivities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestActivities_ProcessActivities_ProcessActivityId",
                table: "RequestActivities",
                column: "ProcessActivityId",
                principalTable: "ProcessActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestActivities_ProcessActivities_ProcessActivityId",
                table: "RequestActivities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RequestActivities",
                table: "RequestActivities");

            migrationBuilder.RenameTable(
                name: "RequestActivities",
                newName: "RequestActivity");

            migrationBuilder.RenameIndex(
                name: "IX_RequestActivities_ProcessActivityId",
                table: "RequestActivity",
                newName: "IX_RequestActivity_ProcessActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RequestActivity",
                table: "RequestActivity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestActivity_ProcessActivities_ProcessActivityId",
                table: "RequestActivity",
                column: "ProcessActivityId",
                principalTable: "ProcessActivities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
